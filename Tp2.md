# TP2 : Routage, DHCP et DNS

Objectifs de ce TP :

- comme toujours, on réutilise les trucs qu'on a vu auparavant
- on appréhende des nouveaux outils et/ou protocoles
  - ici ce sera la mise en place d'un routeur très basique
  - setup d'un serveur DHCP (ui encore) avec une petite option en plus
  - et setup d'un serveur DNS
- je veux vous familiariser avec les choses qu'on voit TOUT LE TEMPS en réseau, et vous faire monter ça vous-mêmes, dans un lab virtuel

On va encore tout faire avec Rocky Linux (ou l'OS de votre choix), toujours la même idée : les systèmes Linux sont des couteaux suisse qu'il est facile de manipuler.


# Sommaire

- [TP2 : Routage, DHCP et DNS](#tp2--routage-dhcp-et-dns)
- [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Routage](#i-routage)
- [II. Serveur DHCP](#ii-serveur-dhcp)
- [III. ARP](#iii-arp)
  - [1. Les tables ARP](#1-les-tables-arp)
  - [2. ARP poisoning](#2-arp-poisoning)

# 0. Setup

Pour chaque VM que vous créerez tout au long du TP :

- [x] Assurez-vous d'avoir un accès SSH fonctionnel
- [x] Donnez un nom d'hôte à la VM (commençons à faire les choses bien, pour ce faire, voir le mémo Rocky)
  - comme ça, quand vous vous connecterez à la VM, y'aura son nom dans le prompt : `[it4@node1]$` par exemple

# I. Routage

➜ **Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `10.2.1.1/24`   |

➜ **Reproduisez la topologie dans votre GNS3**, quelques hints :

- il faudra indiquer à GNS que votre `router.tp2.efrei` a une carte réseau supplémentaire
- le NAT est disponible dans la catégorie "End Devices"
  - il va symboliser un accès internet

☀️ **Configuration de `router.tp2.efrei`**



```bash
[admin@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3c:10:d8 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3c:10d8/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9d:96:6b brd ff:ff:ff:ff:ff:ff
    inet 192.168.250.12/24 brd 192.168.250.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe9d:966b/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f7:dc:46 brd ff:ff:ff:ff:ff:ff

```
```bash
[admin@router ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.254
NETMASK=255.255.255.0

```

☀️ **Configuration de `node1.tp2.efrei`**

```bash
[admin@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ee:a3:08 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feee:a308/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:79:b2:84 brd ff:ff:ff:ff:ff:ff
    inet 192.168.250.11/24 brd 192.168.250.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe79:b284/64 scope link
       valid_lft forever preferred_lft forever
```
```bash
[admin@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0
```
```bash
[admin@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.32 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.742 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.665 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=0.741 ms
^C
--- 10.2.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3053ms
rtt min/avg/max/mdev = 0.665/0.867/1.322/0.264 ms 
```
```bash
[admin@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=4 ttl=116 time=2115 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=116 time=1090 ms
64 bytes from 8.8.8.8: icmp_seq=6 ttl=116 time=66.6 ms
64 bytes from 8.8.8.8: icmp_seq=7 ttl=116 time=17.0 ms
64 bytes from 8.8.8.8: icmp_seq=8 ttl=116 time=15.2 ms
^C
--- 8.8.8.8 ping statistics ---
8 packets transmitted, 5 received, 37.5% packet loss, time 7157ms
rtt min/avg/max/mdev = 15.193/660.771/2114.608/834.555 ms, pipe 3
```

```bash
[admin@node1 ~]$ sudo traceroute -I -q 1 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  *
 2  192.168.122.1 (192.168.122.1)  1.600 ms
 3  10.0.3.2 (10.0.3.2)  7.160 ms
 4  lanspeedtest.wifirst.fr (10.188.0.1)  7.320 ms
 5  172.22.2.1 (172.22.2.1)  6.814 ms
 6  172.21.178.42 (172.21.178.42)  14.694 ms
 7  crco01parth2-googlepni01parth2.core.wifirst.net (46.193.247.53)  14.321 ms
 8  googlepni01parth2-crco01parth2.core.wifirst.net (46.193.247.54)  14.904 ms
 9  108.170.245.1 (108.170.245.1)  15.377 ms
10  142.251.64.127 (142.251.64.127)  13.995 ms
11  dns.google (8.8.8.8)  14.130 ms
```
# II. Serveur DHCP


➜ **Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `N/A`           |
| `dhcp.tp2.efrei`   | `10.2.1.253/24` |

☀️ **Install et conf du serveur DHCP** sur `dhcp.tp2.efrei`

```bash
[admin@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#create new
#specify domain name
option domain-name     "srv.world";

#specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;

#default lease time
default-lease-time 600;

#max lease time
max-lease-time 7200;

#this DHCP server to be declared valid
authoritative;

#specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.220 10.2.1.250;

    option domain-name-servers 8.8.8.8;
    # specify gateway
    option routers 10.2.1.254;
}
```

☀️ **Test du DHCP** sur `node1.tp2.efrei`

```bash
[admin@node1 ~]$ sudo nmcli con reload
[admin@node1 ~]$ sudo nmcli con up enp0s3
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/4)
```
```bash
[admin@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes

```
```bash
[admin@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ee:a3:08 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.220/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 459sec preferred_lft 459sec
    inet6 fe80::a00:27ff:feee:a308/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:79:b2:84 brd ff:ff:ff:ff:ff:ff
    inet 192.168.250.11/24 brd 192.168.250.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe79:b284/64 scope link
       valid_lft forever preferred_lft forever
```
```bash
[admin@node1 ~]$ ping google.com
PING google.com (216.58.213.78) 56(84) bytes of data.
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=1 ttl=116 time=14.5 ms
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=2 ttl=116 time=14.4 ms
64 bytes from par21s18-in-f14.1e100.net (216.58.213.78): icmp_seq=3 ttl=116 time=14.1 ms
64 bytes from lhr25s01-in-f14.1e100.net (216.58.213.78): icmp_seq=4 ttl=116 time=13.7 ms
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=5 ttl=116 time=13.3 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 13.262/13.977/14.499/0.456 ms
```
🌟 **BONUS**

```bash
[admin@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#create new
#specify domain name
option domain-name     "srv.world";

#specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;

#default lease time
default-lease-time 600;

#max lease time
max-lease-time 7200;

#this DHCP server to be declared valid
authoritative;

#specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.220 10.2.1.250;

    option domain-name-servers 8.8.8.8;
    # specify gateway
    option routers 10.2.1.254;
}
```

☀️ **Wireshark it !**

![Alt text](Wireshark1.pcapng)

# III. ARP

## 1. Les tables ARP

ARP est un protocole qui permet d'obtenir la MAC de quelqu'un, quand on connaît son IP.

On connaît toujours l'IP du correspondant avant de le joindre, c'est un prérequis. Quand vous tapez `ping 10.2.1.1`, vous connaissez l'IP, puisque vous venez de la taper :D

La machine va alors automatiquement effectuer un échange ARP sur le réseau, afin d'obtenir l'adresse MAC qui correspond à `10.2.1.1`.

Une fois l'info obtenue, l'info "telle IP correspond à telle MAC" est stockée dans la **table ARP**.


☀️ **Affichez la table ARP de `router.tp2.efrei`**

```bash
[admin@router ~]$ ip n s dev enp0s3
10.2.1.253 lladdr 08:00:27:0e:64:ba STALE
10.2.1.220 lladdr 08:00:27:ee:a3:08 STALE
```

☀️ **Capturez l'échange ARP avec Wireshark**

![Alt text](Wireshark2.pcapng)



